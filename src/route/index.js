import vue from 'vue'
import vueRouter from 'vue-router'
vue.use(vueRouter)

const routers = [
    {
        path: "/",
        name: "home",
        component:()=>import(/*webpackChunkName: "Home"*/"../views/home")
    },
    {
        path: "/products",
        name: "products",
        component:()=>import(/*webpackChunkName: "Home"*/"../views/products")
    },
    {
        path: "/articels",
        name: "articels",
        component:()=>import(/*webpackChunkName: "Home"*/"../views/articles")
    }
]
const router = new vueRouter({
    routers,
});
export default router;